import json

from django.contrib.auth import login, authenticate
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.core import serializers
from orders.forms import SignUpForm

from .models import Food, Category, Profile, Topping, OrderItem, Order

# Create your views here.
def index(request):
    pizza_toppings = Topping.objects.filter(type='PIZZA') | Topping.objects.filter(type='BOTH')
    sub_toppings = Topping.objects.filter(type='SUBS') | Topping.objects.filter(type='BOTH')
    cats = Category.objects.all()
    response_list = []

    if request.user.is_authenticated:
        ordr = Order.objects.filter(user=request.user, status="OPEN")
        if len(ordr) > 0:
            response_list = getorder(ordr[0])

    context = {
        "categories": cats,
        "foods": Food.objects.all(),
        "pizza_toppings": serializers.serialize('json', pizza_toppings, fields=('pk','name')),
        "sub_toppings": serializers.serialize('json', sub_toppings, fields=('pk','name')),
        "open_order": json.dumps(response_list)
    }
    return render(request, "orders/index.html", context)

def setstatus(order_id, status):
    try:.

        order = Order.objects.get(pk = order_id)
        order.status = status
        order.save(update_fields = ['status'])
        return True
    except Exception as e:
        print(e)
        return False

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()  # load the profile instance created by the signal
            user.profile.first_name = form.cleaned_data.get('first_name')
            user.profile.last_name = form.cleaned_data.get('last_name')
            user.profile.email = form.cleaned_data.get('email')
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('index')
    else:
        form = SignUpForm()

    return render(request, "registration/signup.html", {'form': form})

def getfood(request):
    if request.method=='POST':
        try:
            food_id = request.POST.get('food_id')
            food = Food.objects.filter(pk=food_id)
            response_data = serializers.serialize('json', list(food))

            return JsonResponse({'data': response_data})
        except Exception as e:
            print(e)

def addorderitem(request):
    if request.method=='POST':
        try:
            item = json.loads(request.POST.get('data'))
            current_order = Order.objects.filter(user=request.user, status="OPEN")
            if len(current_order) > 0:
                order = current_order[0]
            else:
                order = Order(user=request.user)
                order.save()

            food = Food.objects.get(pk=int(item['food_id']))

            oi = OrderItem(food=food,small=item['small'],quantity=item['quantity'],extra_cheese=item['extra_cheese'], order=order)
            oi.save()

            if len(item['toppings']) > 0:
                for i in item['toppings']:
                    topping = Topping.objects.get(pk=int(i))
                    oi.toppings.add(topping)

            response_list = getorder(order)

            return JsonResponse({'data': response_list, 'status': 200})

        except Exception as e:
            print(e)

def emptybasket(request):
    if request.method=='POST':
        try:
            current_order = Order.objects.get(user=request.user, status="OPEN")
            current_order.delete()
            return JsonResponse({'data': 'Deleted.', 'status': 200})
        except Exception as e:
            print(e)


def placeorder(request):
    ordr = Order.objects.filter(user=request.user, status="OPEN")
    if len(ordr) > 0:
        order = getorder(ordr[0])
    return render(request, "orders/placeorder.html", context={'data':order})

def finalizeorder(request):
    order = Order.objects.get(user=request.user, status="OPEN")
    setstatus(order.pk, "PENDING")
    return redirect('tracker')

def getorder(order):
    response_list = {}
    response_list['items'] = []
    o_order = order
    order_list = OrderItem.objects.filter(order=o_order)
    order_total = 0
    for order_item in order_list:
        response_data = {}
        order_item_price = order_item.food.large_price
        if order_item.small:
            order_item_price = order_item.food.small_price

        order_total += order_item_price

        response_data.update({
            "item_id": order_item.pk,
            "item": f"{order_item.food.category.name} ► {order_item.food.name}",
            "item_small": order_item.small,
            "item_price": order_item_price,
            "item_toppings": []
        })
        for tppng in order_item.toppings.all():
            response_data['item_toppings'].append(tppng.name)

        if order_item.extra_cheese:
            response_data['item_toppings'].append("extra cheese")

        response_list['total'] = order_total
        response_list['items'].append(response_data)
    return response_list

def tracker(request):
    response_list = {}
    orders = Order.objects.filter(user=request.user).exclude(status="OPEN").order_by('order_date').reverse()
    response_list['placed_orders'] = []
    response_list['completed_orders'] = []
    response_list['archived_orders'] = []
    for order in orders:
        ordr = getorder(order)
        order_details = {}
        order_details.update({
            "order_id": order.pk,
            "order_date": order.order_date,
            "order_status": order.status,
            "order_total": ordr['total'],
            "order_items": ordr['items']
        })
        if order.status == 'ARCHIVED':
            response_list['archived_orders'].append(order_details)
        elif order.status == 'COMPLETED':
            response_list['completed_orders'].append(order_details)
        else:
            response_list['placed_orders'].append(order_details)

    return render(request, "orders/tracker.html", response_list)

def checkstatus(request):
    orders = Order.objects.filter(user=request.user).exclude(status="OPEN").order_by('order_date').reverse()

    return JsonResponse({'data': orders[0].status, 'status': 200})

def kitchen(request):
    response = getorders()

    return render(request, "orders/kitchen.html", response)


def getorders():
        response_list = {}
        orders = Order.objects.all().exclude(status="OPEN").order_by('order_date')
        if orders:
            response_list['pending'] = []
            response_list['inprogress'] = []
            response_list['completed'] = []
            response_list['archived'] = []
            response_list['last_id'] = Order.objects.latest('id').pk
            for order in orders:
                ordr = getorder(order)
                order_details = {}
                order_details.update({
                    "order_id": order.pk,
                    "order_date": order.order_date,
                    "order_status": order.status,
                    "order_total": ordr['total'],
                    "order_items": ordr['items']
                })
                if order.status == 'PENDING':
                    response_list['pending'].append(order_details)
                elif order.status == 'IN_PROGRESS':
                    response_list['inprogress'].append(order_details)
                elif order.status == 'COMPLETED':
                    response_list['completed'].append(order_details)
                else:
                    response_list['archived'].append(order_details)

        return response_list

def checkneworder(request):
    pending_orders = getorders()['pending']
    if pending_orders:
        return JsonResponse({'data': pending_orders, 'status': 200})
    else:
        return JsonResponse({'data': None, 'status': 204})

def deleteorder(request):
    if request.method=='POST':
        try:
            order_id = json.loads(request.POST.get('data'))
            Order.objects.get(pk=order_id).delete()
            return JsonResponse({'data': 'success', 'status': 200})
        except Exception as e:
            print(e)
            return JsonResponse({'data': f"error: {e}", 'status': 500})

def setstatusapi(request):
    if request.method=='POST':
        req = json.loads(request.POST.get('data'))
        order_id = req['id']
        status = req['status']
        result = setstatus(int(order_id), status)
        if result:
            return JsonResponse({'data': 'success', 'status': 200})
        else:
            return JsonResponse({'data': "Error", 'status': 500})
