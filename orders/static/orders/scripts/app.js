
document.addEventListener('DOMContentLoaded', () => {
  const order_buttons = document.querySelectorAll('.order-food')
  if(document.querySelector('#pizza-toppings')) {
    var p_toppings = JSON.parse(document.querySelector('#pizza-toppings').textContent)
  }
  if(document.querySelector('#sub-toppings')) {
    var s_toppings = JSON.parse(document.querySelector('#sub-toppings').textContent)
  }
  if(document.querySelector('#open-order')) {
    var open_order = JSON.parse(JSON.parse(document.querySelector('#open-order').textContent))
  }

  order_buttons.forEach(button => {
    button.onclick = function (e) {
      e.preventDefault()
      const food_id = this.parentElement.parentElement.dataset['food']
      const size = this.dataset['size']
      const category = this.parentElement.parentElement.dataset['cat']
      let type = ""
      let toppings_nr = 0

      if(this.parentElement.parentElement.dataset['type']) {
        type = this.parentElement.parentElement.dataset['type']
      }
      if(this.parentElement.parentElement.dataset['tnr']) {
        toppings_nr = this.parentElement.parentElement.dataset['tnr']
      }

      const food = getFood(food_id, function () {
        if(this.status >= 200 && this.status < 400) {
          const fd = JSON.parse(JSON.parse(this.response).data)
          if ((type === 'pizza' & toppings_nr > 0) || type === 'subs') {
            let props = {
              'type': type,
              'category': category,
              'tnr': toppings_nr,
              'food': fd[0],
              'size': size
            }
            showOptions(props)
          } else {
            let foodsmall = false
            if(size == 'small') {
              foodsmall = true
            }
            let data = {
              'food_id': fd[0].pk,
              'small': foodsmall,
              'quantity': 1,
              'toppings': [],
              'extra_cheese': false
            }

            addOrderItem(data)
          }
        } else {
          return false
        }
      })
    }
  })

  getFood = (food_id, onloadFunction, onerrorFunction = function () {}) => {
    const request = new XMLHttpRequest()
    request.open('POST', 'getfood')

    const header =  "X-CSRFToken"
    request.setRequestHeader(header, csrf_token)

    const data = new FormData()
    data.append('food_id', food_id)

    request.onload = onloadFunction
    request.onerror = onerrorFunction

    request.send(data)
  }

  refreshOrderList = (orders) => {
    let total = 0
    let total_li = document.createElement("li")
    let total_span = document.createElement("span")
    let total_amount = document.createElement("span")
    total_span.append('Total:')
    let order_ul = document.createElement("ul")

    orders.forEach(order => {
      total += order.item_price
      let order_li = document.createElement("li")
      let order_name = document.createElement("div")
      let order_price = document.createElement("div")
      let food_title = document.createElement("span")
      let delete_btn = document.createElement("span")

      order_name.classList.add("order-name")
      food_title.append(`${order.item}`)
      order_name.append(food_title)

      if(order.item_small === true) {
        food_title.append(" [small]")
      }

      if(order.item_toppings.length > 0) {
        order_name.append(document.createElement("br"))
        let toppings = document.createElement("span")
        toppings.classList.add("toppings")
        order.item_toppings.forEach((value, index) => {
          toppings.append(value)
          if(index < order.item_toppings.length - 1) {
            toppings.append(", ")
          }
        })
        order_name.append(toppings)
      }

      delete_btn.classList.add('delete')

      order_price.append(`$${order.item_price.toFixed(2)}`)
      order_li.append(order_name)
      order_li.append(order_price)
      order_ul.append(order_li)
    })
    total_amount.append(`$${total.toFixed(2)}`)
    total_li.append(total_span, total_amount)
    order_ul.append(total_li)
    document.querySelector(".order-list").querySelectorAll("*").forEach((el) => {
      el.remove()
    })
    document.querySelector(".order-list").innerHTML = ""
    document.querySelector(".order-list").append(order_ul)
    document.querySelector(".basket-button-container").style.display = "block"
  }

  addOrderItem = (order_item) => {
    const request = new XMLHttpRequest()
    request.open('POST', 'addorderitem')

    const header =  "X-CSRFToken"
    request.setRequestHeader(header, csrf_token)

    const data = new FormData()
    data.append('data', JSON.stringify(order_item))

    request.onload = function () {
      if(this.status >= 200 && this.status < 400) {
        const orders = JSON.parse(this.response).data
        refreshOrderList(orders.items)
      } else {
        console.debug(`Error: ${this.status}`)
      }
    }

    request.onerror = () => {
      return false
    }

    request.send(data)
  }

  showOptions = (props) => {
    let chosen_toppings = 0
    document.querySelector("body").style.overflow = "hidden"
    let topping_list = undefined
    document.querySelector("#backdrop").innerHTML = ""
    const options_window = document.createElement("div")
    options_window.id = "options_window"
    document.querySelector('#backdrop').prepend(options_window)

    if (props['type'] === 'pizza') {
      topping_list = JSON.parse(p_toppings)
    } else if (props['type'] === 'subs') {
      if (props['tnr'] > 0) {
        topping_list = JSON.parse(s_toppings)
      }
    } else {
      return false
    }

    let options_h1 = document.createElement('h3')
    let price = props['size']

    if(props['size'] === 'small') {
      price += ` - $${props['food']['fields'].small_price.toFixed(2)}`
    } else {
      price += ` - $${props['food']['fields'].large_price.toFixed(2)}`
    }
    options_h1.append(`${props['category']} ► ${props['food']['fields'].name}`)
    options_h1_small = document.createElement("small")
    options_h1_small.append(`(${price})`)
    options_window.append(options_h1)
    options_h1.append(options_h1_small)

    let topping_ul = document.createElement('ul')

    if (topping_list !== undefined) {
      topping_list.forEach((topping) => {
        let topping_container = document.createElement('li')
        let topping_option = document.createElement('input')
        topping_option.type = "checkbox"
        topping_option.name = "toptions"
        topping_option.value = topping.pk
        topping_option.id = `option_${topping.pk}`
        topping_option.onchange = function() {
          if(document.querySelectorAll("input[name=toptions]:checked").length > props['tnr'] - 1) {
            document.querySelectorAll("input[name=toptions]").forEach(function(el) {
              if(el.checked === false) {
                el.disabled = true
              }
            })
            add_to_cart.disabled = false
          } else {
            document.querySelectorAll("input[name=toptions]").forEach(function(el) {
              if(el.disabled === true) {
                el.disabled = false
              }
            })
            add_to_cart.disabled = true
          }
        }
        topping_container.append(topping_option)

        let topping_option_label = document.createElement('label')
        topping_option_label.htmlFor = `option_${topping.pk}`
        topping_option_label.append(topping.fields.name);
        topping_container.append(topping_option_label)
        topping_ul.append(topping_container)
      })
      document.querySelector('#options_window').append(topping_ul)
    }

    let options_btns_cont = document.createElement('div')
    options_btns_cont.classList.add('container')

    let options_btns = document.createElement('div')
    options_btns.classList.add('row', 'justify-content-between')

    let cancel = document.createElement('button')
    cancel.append('Cancel')
    cancel.classList.add('btn', 'btn-outline-danger')

    cancel.onclick = () => {
      hideOptions()
    }

    options_btns.append(cancel)

    let add_to_cart = document.createElement('button')

    if (props['type'] === 'subs') {
      let extra_cheese_container = document.createElement('div')
      extra_cheese_container.classList.add('extra-cheese-container')
      let extra_cheese = document.createElement('input')
      extra_cheese.type = 'checkbox'
      extra_cheese.id = 'extra-cheese'
      let extra_cheese_label = document.createElement('label')
      extra_cheese_label.htmlFor = 'extra-cheese'
      extra_cheese_label.append("Extra cheese")
      extra_cheese_container.append(extra_cheese, extra_cheese_label)
      document.querySelector('#options_window').append(extra_cheese_container)
    }

    add_to_cart.append('Add to order')
    add_to_cart.classList.add('btn', 'btn-outline-success')
    add_to_cart.id = "add_to_cart"

    if(props['tnr'] == 0 && props['type'] === 'subs') {
      add_to_cart.disabled = false
    } else {
      add_to_cart.disabled = true
    }

    add_to_cart.onclick = () => {
      let foodsmall = false
      if(props['size'] == 'small') {
        foodsmall = true
      }

      let tppngs = []
      document.querySelectorAll("input[name=toptions]:checked").forEach((tpp) => {
        tppngs.push(tpp.value)
      })

      let xcheese = false

      if(document.querySelector("#extra-cheese")) {
        if(document.querySelector("#extra-cheese").checked) {
          xcheese = true
        }
      }

      let data = {
        'food_id': props['food'].pk,
        'small': foodsmall,
        'quantity': 1,
        'toppings': tppngs,
        'extra_cheese': xcheese
      }
      addOrderItem(data)
      hideOptions()
    }

    options_btns.append(add_to_cart)
    options_btns_cont.append(options_btns)
    options_window.append(options_btns_cont)

    document.querySelector("#backdrop").style.display = 'block'
  }

  hideOptions = () => {
    document.querySelector("#backdrop").querySelectorAll("*").forEach((el) => {
      el.remove()
    })
    document.querySelector("#backdrop").style.display = 'none'
    document.querySelector("body").style.overflow = "auto"
  }

  if(document.querySelector(".empty-basket")) {
    document.querySelector(".empty-basket").onclick = () => {
      const request = new XMLHttpRequest()
      request.open('POST', 'emptybasket')

      const header =  "X-CSRFToken"
      request.setRequestHeader(header, csrf_token)

      request.onload = () => {
        document.querySelector(".order-list").innerHTML = "Your basket is empty."
        document.querySelector(".basket-button-container").style.display = "none"
      }

      request.onerror = () => {}

      request.send()
    }
  }

  if(document.querySelector(".menu")) {
    document.querySelector(".menu").onscroll = () => {
      if(document.querySelector(".menu").scrollTop < 1815) {
        document.querySelector(".nose").style.width = (document.querySelector(".menu").scrollTop / 2) + 50 + "px"
      }
    }
  }

  tracker = () => {
    const request = new XMLHttpRequest()
    request.open('POST', 'checkstatus')
    const header =  "X-CSRFToken"
    request.setRequestHeader(header, csrf_token)
    request.onload = function () {
      if(this.status >= 200 && this.status < 400) {
        status = JSON.parse(this.response).data
        if(status == 'PENDING') {
          document.querySelector('body').classList.remove('completed', 'in_progress')
          document.querySelector('body').classList.add('pending')
        } else if(status == 'IN_PROGRESS') {
          document.querySelector('body').classList.remove('completed', 'pending')
          document.querySelector('body').classList.add('in_progress')
        } else {
          document.querySelector('body').classList.remove('pending', 'in_progress')
          document.querySelector('body').classList.add('completed')
        }

        if(status !== 'COMPLETED') {
          setTimeout(function () {
            tracker()
          }, 2000)
        }
      } else {
        console.debug(`Error: ${this.status}`)
      }
    }

    request.onerror = () => {}

    request.send()
  }

  if(open_order !== undefined && open_order.items !== undefined) {
    if(open_order.items.length > 0) {
      refreshOrderList(open_order.items)
    }
  }
})
