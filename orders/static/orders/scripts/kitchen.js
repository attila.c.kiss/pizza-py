document.addEventListener('DOMContentLoaded', () => {
  checkNewOrder = () => {
    const request = new XMLHttpRequest()
    request.open('POST', 'checkneworder')

    const header =  "X-CSRFToken"
    request.setRequestHeader(header, csrf_token)

    request.onload = function () {
      if(this.status >= 200 && this.status < 400) {
        if(this.status == 200) {
          pending_orders = JSON.parse(this.response).data
          if(pending_orders !== null ) {
            pending_orders.forEach(p_order => {
              if(p_order.order_id > last_id) {
                const new_card = document.createElement('div')
                new_card.classList.add('card')
                new_card.dataset['id'] = p_order.order_id
                const card_header = document.createElement('h5')
                card_header.classList.add('card-header')
                card_header.appendChild(document.createTextNode(`#${p_order.order_id}`))
                const order_list = document.createElement('ul')
                order_list.classList.add('list-group', 'list-group-flush')
                p_order.order_items.forEach(o_item => {
                  const list_item = document.createElement('li')
                  list_item.classList.add('list-group-item')
                  const h6 = document.createElement('h6')
                  if(o_item.item_small) {
                    h6.appendChild(document.createTextNode(`Small ${o_item.item}`))
                  } else {
                    h6.appendChild(document.createTextNode(o_item.item))
                  }
                  list_item.append(h6)
                  const toppings = document.createElement('div')
                  let i = 0
                  o_item.item_toppings.forEach(tppng => {
                    if(i > 0) {
                      toppings.appendChild(document.createTextNode(', '))
                    }
                    toppings.appendChild(document.createTextNode(tppng))
                    i++
                  })

                  list_item.append(toppings)
                  order_list.append(list_item)
                })
                const btn_container = document.createElement('div')
                btn_container.classList.add('card-body', 'd-flex', 'justify-content-between', 'btn-cont')

                const btn_del = document.createElement('button')
                btn_del.classList.add('btn', 'btn-danger', 'btn-delete')
                btn_del.onclick = function (e) {
                  e.preventDefault()
                  deleteOrder(this.parentElement.parentElement.dataset['id'])
                }
                btn_del.appendChild(document.createTextNode('Delete'))

                btn_container.append(btn_del)

                const btn_inp = document.createElement('button')
                btn_inp.classList.add('btn', 'btn-success', 'btn-in-progress')
                btn_inp.onclick = function (e) {
                  e.preventDefault()
                  setStatus(this.parentElement.parentElement.dataset['id'], 'IN_PROGRESS')
                }
                btn_inp.appendChild(document.createTextNode('In progress »'))

                btn_container.append(btn_inp)

                new_card.append(card_header)
                new_card.append(order_list)
                new_card.append(btn_container)
                last_id = p_order.order_id
                document.querySelector('.pending-tickets .ticket-container').append(new_card)
              }

            })
          }
        }
      } else {
        console.debug(`Error: ${this.status}`)
      }
      setTimeout(checkNewOrder, 2000)
    }

    request.onerror = () => {
      return false
    }
    request.send()
  }

  deleteOrder = (id) => {
    const request = new XMLHttpRequest()
    request.open('POST', 'deleteorder')

    const header =  "X-CSRFToken"
    request.setRequestHeader(header, csrf_token)

    const data = new FormData()
    data.append('data', JSON.stringify(id))

    request.onload = function () {
      if(this.status >= 200 && this.status < 400) {
        if(this.status == 200) {
          document.querySelector(`[data-id='${id}']`).remove()
        }

      } else {
        console.debug(`Error: ${this.status}`)
      }
    }

    request.onerror = () => {
      return false
    }

    request.send(data)
  }

  setStatus = (id, status) => {
    const request = new XMLHttpRequest()
    request.open('POST', 'setstatusapi')
    const btn_del = document.createElement('button')
    btn_del.classList.add('btn', 'btn-danger', 'btn-delete')
    btn_del.onclick = function (e) {
      e.preventDefault()
      deleteOrder(this.parentElement.parentElement.dataset['id'])
    }
    btn_del.appendChild(document.createTextNode('Delete'))

    const btn_arch = document.createElement('button')
    btn_arch.classList.add('btn', 'btn-primary', 'btn-archived')
    btn_arch.onclick = function (e) {
      e.preventDefault()
      setStatus(this.parentElement.parentElement.dataset['id'], 'ARCHIVED')
    }
    btn_arch.appendChild(document.createTextNode('Archive'))

    const btn_b_pend = document.createElement('button')
    btn_b_pend.classList.add('btn', 'btn-secondary', 'btn-pending')
    btn_b_pend.onclick = function (e) {
      e.preventDefault()
      setStatus(this.parentElement.parentElement.dataset['id'], 'PENDING')
    }
    btn_b_pend.appendChild(document.createTextNode('« Back to pending'))

    const btn_inp = document.createElement('button')
    btn_inp.classList.add('btn', 'btn-success', 'btn-in-progress')
    btn_inp.onclick = function (e) {
      e.preventDefault()
      setStatus(this.parentElement.parentElement.dataset['id'], 'IN_PROGRESS')
    }
    btn_inp.appendChild(document.createTextNode('In progress »'))

    const btn_b_inp = document.createElement('button')
    btn_b_inp.classList.add('btn', 'btn-secondary', 'btn-in-progress')
    btn_b_inp.onclick = function (e) {
      e.preventDefault()
      setStatus(this.parentElement.parentElement.dataset['id'], 'IN_PROGRESS')
    }
    btn_b_inp.append('« Back to in progress')

    const btn_comp = document.createElement('button')
    btn_comp.classList.add('btn', 'btn-success', 'btn-completed')
    btn_comp.onclick = function (e) {
      e.preventDefault()
      setStatus(this.parentElement.parentElement.dataset['id'], 'COMPLETED')
    }
    btn_comp.append('Completed »')

    const header =  "X-CSRFToken"
    request.setRequestHeader(header, csrf_token)

    const data = new FormData()
    data.append('data', JSON.stringify({'id': id, 'status': status}))

    request.onload = function () {
      const response = JSON.parse(this.response).data
      console.debug(response)
      if(this.status >= 200 && this.status < 400) {
        if(this.status == 200) {
          const card = document.querySelector(`[data-id='${id}']`)

          let btn_container = card.getElementsByClassName('btn-cont')[0]
          btn_container.innerHTML = ""

          switch (status) {
            case 'PENDING':
            card.remove()
            document.querySelector('.pending-tickets .ticket-container').append(card)
            btn_container.append(btn_del, btn_inp)
            break
            case 'IN_PROGRESS':
            card.remove()
            document.querySelector('.in-progress-tickets .ticket-container').append(card)
            btn_container.append(btn_b_pend, btn_comp)
            break
            case 'COMPLETED':
            card.remove()
            document.querySelector('.completed-tickets .ticket-container').prepend(card)
            btn_container.append(btn_b_inp, btn_arch)
            break
            default:
            card.remove()
            console.debug(`Order #${id} has been archived.`)
          }
        }
      } else {
        console.debug(`Error: ${this.status}`)
      }
    }

    request.onerror = () => {
      return false
    }

    request.send(data)
  }

  document.querySelectorAll('.btn-delete-order').forEach(btn => {
    btn.onclick = function (e) {
      e.preventDefault()
      deleteOrder(btn.parentElement.parentElement.dataset['id'])
    }
  })

  document.querySelectorAll('.btn-in-progress').forEach(btn => {
    btn.onclick = function (e) {
      e.preventDefault()
      setStatus(btn.parentElement.parentElement.dataset['id'], 'IN_PROGRESS')
    }
  })

  document.querySelectorAll('.btn-pending').forEach(btn => {
    btn.onclick = function (e) {
      e.preventDefault()
      setStatus(btn.parentElement.parentElement.dataset['id'], 'PENDING')
    }
  })

  document.querySelectorAll('.btn-completed').forEach(btn => {
    btn.onclick = function (e) {
      e.preventDefault()
      setStatus(btn.parentElement.parentElement.dataset['id'], 'COMPLETED')
    }
  })

  document.querySelectorAll('.btn-archived').forEach(btn => {
    btn.onclick = function (e) {
      e.preventDefault()
      setStatus(btn.parentElement.parentElement.dataset['id'], 'ARCHIVED')
    }
  })

  checkNewOrder()
})
