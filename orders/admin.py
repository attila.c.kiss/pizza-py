from django.contrib import admin
from .models import Category, Food, Topping, OrderItem, Order

class FoodInline(admin.TabularInline):
    model = Food
    fields = ['category']
    extra = 1

class CategoryAdmin(admin.ModelAdmin):
    inlines = [FoodInline,]
    list_display = ('name',)

class FoodAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'small_price', 'large_price', 'toppings', 'category')

class ToppingAdmin(admin.ModelAdmin):
    list_display = ('name', 'type')

# Register your models here.
admin.site.register(Food, FoodAdmin)
admin.site.register(Topping, ToppingAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(OrderItem)
admin.site.register(Order)
