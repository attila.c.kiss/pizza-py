from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("signup", views.signup, name="signup"),
    path("getfood", views.getfood, name='getfood'),
    path("addorderitem", views.addorderitem, name='addorderitem'),
    path("emptybasket", views.emptybasket, name='emptybasket'),
    path("placeorder", views.placeorder, name='placeorder'),
    path("finalizeorder", views.finalizeorder, name='finalizeorder'),
    path("tracker", views.tracker, name='tracker'),
    path("kitchen", views.kitchen, name='kitchen'),
    path("checkstatus", views.checkstatus, name='checkstatus'),
    path("deleteorder", views.deleteorder, name='deleteorder'),
    path("setstatusapi", views.setstatusapi, name='setstatusapi'),
    path("checkneworder", views.checkneworder, name='checkneworder')
]
