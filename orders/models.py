from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    email = models.EmailField('Email Address')

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()


class Category(models.Model):
    name = models.CharField(max_length=64)

    def __str__(self):
        return self.name

class Food(models.Model):
    name = models.CharField(max_length=64)
    small_price = models.FloatField(null=True, default=None, blank=True)
    large_price = models.FloatField()
    toppings = models.PositiveSmallIntegerField(default=0)
    category = models.ForeignKey(Category, null=True, on_delete=models.CASCADE, related_name="category")

    def __str__(self):
        return f"{self.name} (small: ${self.small_price}, large: ${self.large_price})"

TOPPING_CHOICES = (
    ('PIZZA', 'pizza'),
    ('SUBS', 'subs'),
    ('BOTH', 'both')
)

class Topping(models.Model):
    name = models.CharField(max_length=64)
    type = models.CharField(max_length=5, choices=TOPPING_CHOICES, default='PIZZA')

    def __str__(self):
        return f"{self.name}"

STATUS_CHOICES = (
    ('OPEN', 'open'),
    ('PENDING', 'pending'),
    ('IN_PROGRESS', 'in progress'),
    ('COMPLETED', 'completed'),
    ('ARCHIVED', 'archived')
)

class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user")
    order_date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=11, choices=STATUS_CHOICES, default="OPEN")

class OrderItem(models.Model):
    food = models.ForeignKey(Food, on_delete=models.CASCADE, related_name="food")
    small = models.BooleanField(default=False)
    quantity = models.PositiveSmallIntegerField(default=1)
    toppings = models.ManyToManyField(Topping, blank=True, related_name="toppings")
    extra_cheese = models.BooleanField(default=False)
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name="order")

    def __str__(self):
        small = ""

        if self.small == True:
            size = " (small)"

        return f"{self.quantity}x {self.food}{small} - {self.toppings}"
